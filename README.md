# Discourse operator for OKD

## Installing the Crunchydata Postgresql Operator
- `mkdir -p $HOME/odev/src/github.com/crunchydata $HOME/odev/bin $HOME/odev/pkg`
- `cd $HOME/odev/src/github.com/crunchydata`
- `git clone https://github.com/CrunchyData/postgres-operator.git`
- `cd postgres-operator`
- `git checkout 4.0.0`
- `cat $HOME/odev/src/github.com/crunchydata/postgres-operator/examples/envs.sh >> $HOME/.bashrc`
- `source $HOME/.bashrc`
- `export NAMESPACE=pgouser1,pgouser2`
- `export PGO_OPERATOR_NAMESPACE=pgo`
- `make setupnamespaces`
- Change the storage options in `conf/postgresql-operator/pgo.yaml` to `hostpathstorage`
Change from
```
PrimaryStorage: storageos
BackupStorage: storageos
ReplicaStorage: storageos
BackrestStorage: storageos
```
to
```
PrimaryStorage: hostpathstorage
BackupStorage: hostpathstorage
ReplicaStorage: hostpathstorage
BackrestStorage: hostpathstorage
```
- Install expenv
`wget https://github.com/blang/expenv/releases/download/v1.2.0/expenv_amd64.tar.gz`
`tar -xzf expenv_amd64.tar.gz expenv`
`cp expenv /usr/bin`
- `cp ./conf/postgres-operator/pgouser $HOME/.pgouser`
- `cp ./conf/postgres-operator/pgorole $HOME/.pgorole`
- `make installrbac`
- `make deployoperator`
- `wget https://github.com/CrunchyData/postgres-operator/releases/download/4.0.0/pgo -O /usr/bin/pgo` - Since the version of postgres operator is 4.0.0
- `chmod 777 /usr/bin/pgo` - Give executable permissions to the downloaded 'pgo' file
<!-- - `oc get service postgres-operator -n pgo` -->
- `IP=$(oc get svc postgres-operator -n pgo -o jsonpath='{range.spec}{.clusterIP}')`
- `export PGO_APISERVER_URL=https://$(oc get svc postgres-operator -n pgo -o jsonpath='{range.spec}{.clusterIP}'):8443`
- `pgo version`
- `pgo create cluster mycluster -n pgouser1`
- `pgo show cluster mycluster -n pgouser1`
- `pgo create user user1 --selector=name=mycluster --pasword=newpass` - Create a new user with a password
- `pgo user --change-password=postgres --selector=name=mycluster --password=newpass` - Updated an existing user with a given password
- `pgo test mycluster -n pgouser1` - Testing the cluster
- `pgo scale mycluster -n pgouser1` - Scaling the cluster
- 
## Installing the Discourse Operator
- `git clone https://gitlab.cern.ch/rvineetr/discourse-operator.git`
- `cd discourse-operator`
- `oc create -f deploy/crds/discourse_v1alpha1_discourse_crd.yaml`
- `oc create -f deploy/`
- `oc adm policy add-cluster-role-to-user admin -z discourse-operator`
- Use the username, password from above, hostname to be `mycluster.pgouser1.svc` in the file `deploy/crds/discourse_v1alpha1_discourse_cr.yaml`
- `oc create -f deploy/crds/discourse_v1alpha1_discourse_cr.yaml`
 
Docmentation - https://codimd.web.cern.ch/s/Hku5hnRQr#

